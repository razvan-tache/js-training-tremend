var items = [
    {
        id: 1,
        name: "Item 1",
        owner: 1,
        details: {
            description: 'Lorem ipsum'
        },
        filesTypes: [
            {
                file_id: 1,
                filename: "file1.obj",
                extension: "obj",
                distributions: [
                    {
                        id: 1,
                        name: "Shapeways",
                        price: 100
                    }
                ]
            }
        ],
        collections: [1],
        parts: [2],
        tags: [1]
    }
];